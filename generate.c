
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

static const char charset[]=
  "0123456789"
  "abcdefghijklmnopqrstuvwxyz"
  "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
  ".-+:~";

#define ML 8

static char sofar[ML+1];

static int denomix, denom=1;

static void gen(char *inbuf, int len) {
  if (!len) {
    *inbuf = 0;
    if (!denomix) {
      puts(sofar);
    }
    denomix += (denom-1);
    denomix %= denom;
    return;
  }
  const char *cp;
  for (cp=charset; *cp; cp++) {
    *inbuf = *cp;
    gen(inbuf+1, len-1);
  }
}    

int main(int argc, char **argv) {
  int len;
  int minlen=0, maxlen=ML;
  char *slash;

  if (argc>=2 && (slash = strchr(argv[1], '/'))) {
      denomix = atoi(argv[1]);
      denom = atoi(slash+1);
      argv++; argc--;
  }

  if (argc==3) {
    minlen = atoi(argv[1]);
    maxlen = atoi(argv[2]);
  } else if (argc==2) {
    maxlen = atoi(argv[1]);
  } else if (argc==1) {
  } else {
    abort();
  }

  assert(maxlen <= ML);

  for (len=minlen; len<=maxlen; len++) {
    fprintf(stderr,"********** generating len=%d **********\n",len);
    gen(sofar, len);
  }
  if (ferror(stdout) ||
      fflush(stdout)) { perror("stdout"); abort(); }
  fputs("generate done\n",stderr);
  return 0;
}
